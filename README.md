# TERN Organisations and Persons

This repository is the point-of-truth for the ontology, the SHACL shape, and the RDF data of organisations and persons who are affiliated with TERN.

A mirror-copy of the data here will be made available in TERN's triple-store to enable SPARQL-endpoint access to the data. 

This repository is developed using TopBraid Composer. The *files of interest* should be loaded in to the *project*. As data is entered using TopBraid Composer, it is useful to run the inferencer to automatically write additional data based on the ontology. Each data entry must run the SHACL validator to ensure conformance to the ontology. 

The data is version-controlled with Git. Data should only enter the *master* branch when a pull request has been approved by an administrator. 

**Details on governance workflows will be further expanded here in the future.**


## SHACL Shapes

A class-level view of the TERN Organisations and Persons SHACL shapes. The below describes the constraints of the data. 

![TERN Organisations and Persons Ontology diagram](/org_person_ont_diagram.png)

Or view the [SVG](org_person_ont_diagram.svg) version.


## Ontology

The ontology is a small subset of:

- schema.org ontology for persons, organisations, addresses
- W3C Organization ontology for change event
- and some specialised properties under the TERN namespace (such as a property for AGOR).


## Files of Interest

- [org_person_ont.ttl](org_person_ont.ttl) - TERN Organisations and Persons ontology
- [shapes.ttl](shapes.ttl) - SHACL shape constraints
- [data.ttl](data.ttl) - TERN organisations, persons, addresses, and change events, in RDF.
- [org_class_terms.ttl](org_class_terms.ttl) - Organisation classifcation terms. 


## Contact

**Edmond Chuc**  
*Software engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  
